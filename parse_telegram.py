import requests
from bs4 import BeautifulSoup


def get_html(url):
    r = requests.get(url)
    return r.text


def parse_page_data(html):
    result = []
    soup = BeautifulSoup(html, 'lxml')
    divs = soup.find('div', class_='region-content')
    ads = divs.find_all('div', class_='w-item')
    for ad in ads:
        try:
            div = ad.find('p', class_='g-no')
            number = div.text.strip()
        except:
            number = ""
        try:
            div = ad.find('h3')
            title = div.text.strip()
        except:
            title = ""
        try:
            div = ad.find_all('span')
            date_end = div[2].text.strip()
        except:
            date_end = ""

        pars_item = {'number': number,
                     'title': title,
                     'date_end': date_end}
        result.append(pars_item)
    return result


def get_page_data():
    url = "http://rn.tektorg.ru/ru/procurement/procedures?limit=100&sort_by=DATE&sort_order=DESC&key_words=&proc=&organizations=%255B%2522%25D0%2590%25D0%25BA%25D1%2586%25D0%25B8%25D0%25BE%25D0%25BD%25D0%25B5%25D1%2580%25D0%25BD%25D0%25BE%25D0%25B5%2520%25D0%25BE%25D0%25B1%25D1%2589%25D0%25B5%25D1%2581%25D1%2582%25D0%25B2%25D0%25BE%2520%255C%2522%25D0%259A%25D1%2583%25D0%25B9%25D0%25B1%25D1%258B%25D1%2588%25D0%25B5%25D0%25B2%25D1%2581%25D0%25BA%25D0%25B8%25D0%25B9%2520%25D0%25BD%25D0%25B5%25D1%2584%25D1%2582%25D0%25B5%25D0%25BF%25D0%25B5%25D1%2580%25D0%25B5%25D1%2580%25D0%25B0%25D0%25B1%25D0%25B0%25D1%2582%25D1%258B%25D0%25B2%25D0%25B0%25D1%258E%25D1%2589%25D0%25B8%25D0%25B9%2520%25D0%25B7%25D0%25B0%25D0%25B2%25D0%25BE%25D0%25B4%255C%2522%2522%255D&ogranizator-id=%D0%90%D0%BA%D1%86%D0%B8%D0%BE%D0%BD%D0%B5%D1%80%D0%BD%D0%BE%D0%B5+%D0%BE%D0%B1%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%BE+%22%D0%9A%D1%83%D0%B9%D0%B1%D1%8B%D1%88%D0%B5%D0%B2%D1%81%D0%BA%D0%B8%D0%B9+%D0%BD%D0%B5%D1%84%D1%82%D0%B5%D0%BF%D0%B5%D1%80%D0%B5%D1%80%D0%B0%D0%B1%D0%B0%D1%82%D1%8B%D0%B2%D0%B0%D1%8E%D1%89%D0%B8%D0%B9+%D0%B7%D0%B0%D0%B2%D0%BE%D0%B4%22&proc_type=21&status_lot=2&published_from=&published_to=&end_reg_from=&end_reg_to=&okveds=&okdps=#expand"
    html = get_html(url)
    return parse_page_data(html)
