import telebot
import requests
from parse_telegram import get_page_data

# from telebot import apihelper
# apihelper.proxy = {'https': '23.237.173.102:3128'}

bot = telebot.TeleBot("963482560:AAGYgwvluKX-dyvt7WC_iiTHt4_ltfr386U")


@bot.message_handler(commands=["help", "start"])
def message_handler(message):
    bot.send_message(message.from_user.id, "Привет вот команды какие я могу выполнять \n" +
                     "/exchange - Получить информацию о курсе валют USD, EUR \n" +
                     "/parse - Получить информацию с торговой площадки РОСНЕФТЬ, Куйбышевский НПЗ")


@bot.message_handler(commands=["parse"])
def parse_handler(message):
    data = get_page_data()
    for item in data:
        bot.send_message(message.from_user.id, "Лот - {} \n".format(item["number"]) + "{} \n".format(
            item["title"]) + "Дата окончания приема заявок - {}".format(item["date_end"]))


@bot.message_handler(commands=["exchange"])
def message_handler(message):
    bot.send_message(message.chat.id, "Выберите валюту", reply_markup=gen_keyboard())


@bot.callback_query_handler(func=lambda call: True)
def iq_callback(query):
    bot.answer_callback_query(query.id)
    bot.send_message(query.message.chat.id, get_exchange(query.data))


def gen_keyboard():
    keyboard = telebot.types.InlineKeyboardMarkup()
    keyboard.row(
        telebot.types.InlineKeyboardButton("EUR", callback_data="EURRUB"),
        telebot.types.InlineKeyboardButton("USD", callback_data="USDRUB")
    )
    return keyboard


def get_exchange(text):
    res = requests.get("https://currate.ru/api/?get=rates&key=9aed0dfc55206fe9fca03165d0cfa753",
                       params={"pairs": text})
    return "Текущий курс - {} руб.".format(res.json()["data"][text])


bot.polling(none_stop=True)
